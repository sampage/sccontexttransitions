//
//  UIView+SCCurvePathAnimation.h
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBezierPath+SCCurvePath.h"

@interface UIView (SCCurvePathAnimation)

- (void)animateAlongCurveToFrame:(CGRect)toFrame apex:(CGFloat)apex duration:(NSTimeInterval)duration;

@end
