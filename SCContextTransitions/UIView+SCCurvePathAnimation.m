//
//  UIView+SCCurvePathAnimation.m
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import "UIView+SCCurvePathAnimation.h"
#import "UIBezierPath+SCCurvePath.h"

@implementation UIView (SCCurvePathAnimation)

- (void)animateAlongCurveToFrame:(CGRect)toFrame apex:(CGFloat)apex duration:(NSTimeInterval)duration
{
    // CALayer position is relative to the layers anchor point
    CGPoint fromPosition = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    CGPoint toPosition = CGPointMake(CGRectGetMidX(toFrame), CGRectGetMidY(toFrame));
    
    // Create an animate to move the view along the path
    CAKeyframeAnimation *positionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    positionAnimation.path = [[UIBezierPath curvedPathFromPoint:fromPosition toPoint:toPosition apex:apex] CGPath];
    
    // Create an animate to adjust the view size
    CABasicAnimation *boundsAnimation = [CABasicAnimation animationWithKeyPath:@"bounds"];
    boundsAnimation.fromValue = [NSValue valueWithCGRect:self.bounds];
    
    //  Create an animation group to perform the size and position animations
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.animations = @[positionAnimation, boundsAnimation];
    animationGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animationGroup.duration = duration;
    
    // Update the layer model
    self.layer.position = toPosition;
    self.layer.bounds = CGRectMake(0.f, 0.f, CGRectGetWidth(toFrame), CGRectGetHeight(toFrame));
    
    // Add the animation to the layer
    [self.layer addAnimation:animationGroup forKey:@"frameAnimation"];
}

@end
