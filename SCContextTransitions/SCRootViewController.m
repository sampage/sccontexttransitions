//
//  SCRootViewController.m
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import "SCRootViewController.h"
#import "SCDetailViewController.h"

#import "SCMovesPresentTransition.h"
#import "SCMovesDismissTransition.h"

#import "SCDataItemCollectionViewCell.h"

#import "SCCollectionViewDataSource.h"

@interface SCRootViewController () <UIViewControllerTransitioningDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) SCCollectionViewDataSource *dataSource;

@property (nonatomic, strong) UIView *selectedContextView;

@end

@implementation SCRootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    self.dataSource = [[SCCollectionViewDataSource alloc] init];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.dataSource = self.dataSource;
    self.collectionView.delegate = self;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    
    [self.collectionView registerClass:[SCDataItemCollectionViewCell class] forCellWithReuseIdentifier:kSCCollectionViewCellReuseIdentifier];
    
    [self.view addSubview:self.collectionView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGFloat collectionViewHeight = CGRectGetHeight(self.view.bounds) / 3.f;
    self.collectionView.frame = CGRectMake(0.f, CGRectGetMidY(self.view.bounds) - (collectionViewHeight / 2.f), CGRectGetWidth(self.view.bounds), collectionViewHeight);
}

#pragma mark - SCContextTransitioning

- (void)insertContextViewInViewHierarchy:(UIView *)contextView
{
    [self.collectionView reloadData];
}

- (CGRect)frameForContextViewInWindowCoordinates:(UIView *)contextView
{
    NSIndexPath *indexPathForCell = [self.dataSource indexPathForContextView:contextView];
    SCDataItemCollectionViewCell *collectionViewCell = (SCDataItemCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPathForCell];
    
    CGRect frameForContextView = [collectionViewCell frameForContextView];
    CGRect frameConvertedToWindow = [collectionViewCell convertRect:frameForContextView toView:nil];
    
    return frameConvertedToWindow;
}

- (void)presentDetailViewController
{
    SCDetailViewController *detailViewController = [[SCDetailViewController alloc] initWithNibName:nil bundle:nil];
    detailViewController.transitioningDelegate = self;
    detailViewController.modalPresentationStyle = UIModalPresentationCustom;
    
    [self presentViewController:detailViewController
                       animated:YES
                     completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    return [[SCMovesPresentTransition alloc] initWithContextView:self.selectedContextView];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return [[SCMovesDismissTransition alloc] initWithContextView:self.selectedContextView];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SCDataItemCollectionViewCell *cell = (SCDataItemCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    self.selectedContextView = cell.contextView;
    
    [self presentDetailViewController];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(collectionView.bounds) / 6.f, CGRectGetHeight(collectionView.bounds));
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.f;
}

@end
