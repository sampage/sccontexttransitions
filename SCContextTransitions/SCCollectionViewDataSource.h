//
//  SCCollectionViewDataSource.h
//  SCContextTransitions
//
//  Created by sampage on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * kSCCollectionViewCellReuseIdentifier;

@interface SCCollectionViewDataSource : NSObject <UICollectionViewDataSource>

- (NSIndexPath *)indexPathForContextView:(UIView *)contextView;

@end
