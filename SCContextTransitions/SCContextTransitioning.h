//
//  SCContextTransitioning.h
//  SCContextTransitions
//
//  Created by sampage on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SCContextTransitioning <NSObject>

- (id)initWithContextView:(UIView *)contextView;

@end
