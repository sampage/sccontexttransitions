//
//  SCResizingCircleView.h
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import <UIKit/UIKit.h>

/* This class is to be replaced with a more 
 * robust solution that handles scaling in more
 * appropriate fashion 
 */

@interface SCResizingCircleView : UIView

@end
