//
//  SCMovesDismissTransition.m
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import "SCMovesDismissTransition.h"
#import "SCContextTransitioningViewController.h"

#import "UIView+SCCurvePathAnimation.h"

@interface SCMovesDismissTransition ()

@property (nonatomic, strong) UIView *contextView;

@end

@implementation SCMovesDismissTransition

@synthesize contextView = _contextView;

- (id)initWithContextView:(UIView *)contextView
{
    if (self = [super init])
    {
        self.contextView = contextView;
    }
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.4f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController<SCContextTransitioningViewController> *fromViewController = (UIViewController<SCContextTransitioningViewController> *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController<SCContextTransitioningViewController> *toViewController = (UIViewController<SCContextTransitioningViewController> *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];

    UIView *containerView = [transitionContext containerView];
    
    NSTimeInterval transitionDuration = [self transitionDuration:transitionContext];
    
    CGRect convertedFromFrame = [self.contextView.superview convertRect:self.contextView.frame toView:containerView];
    self.contextView.frame = convertedFromFrame;
    
    [self.contextView removeFromSuperview];
    [containerView addSubview:self.contextView];
    
    CGRect contextViewToFrame = [toViewController frameForContextViewInWindowCoordinates:self.contextView];
    [self.contextView animateAlongCurveToFrame:contextViewToFrame apex:75.f duration:transitionDuration];
    
    [UIView animateWithDuration:transitionDuration
                     animations:^{
                         fromViewController.view.alpha = 0.f;
                         fromViewController.view.transform = CGAffineTransformMakeScale(0.8f, 0.8f);
                     } completion:^(BOOL finished) {
                         BOOL transitionWasCancelled = [transitionContext transitionWasCancelled];
                         [transitionContext completeTransition:transitionWasCancelled == NO];
                         
                         if (transitionWasCancelled == NO)
                         {
                             [toViewController insertContextViewInViewHierarchy:self.contextView];
                         }
                     }];
}

@end
