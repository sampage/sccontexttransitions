//
//  UIBezierPath+SCCurvePath.h
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBezierPath (SCCurvePath)

+ (UIBezierPath *)curvedPathFromPoint:(CGPoint)fromPoint toPoint:(CGPoint)toPoint apex:(CGFloat)apex;

@end
