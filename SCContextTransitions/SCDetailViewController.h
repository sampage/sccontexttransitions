//
//  SCDetailViewController.h
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCContextTransitioningViewController.h"

@interface SCDetailViewController : UIViewController <SCContextTransitioningViewController>

@end
