//
//  SCMovesPresentTransition.m
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import "SCMovesPresentTransition.h"
#import "SCContextTransitioningViewController.h"

#import "UIView+SCCurvePathAnimation.h"

@interface SCMovesPresentTransition ()

@property (nonatomic, strong) UIView *contextView;

@end

@implementation SCMovesPresentTransition

- (id)initWithContextView:(UIView *)contextView
{
    if (self = [super init])
    {
        self.contextView = contextView;
    }
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return .4f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController<SCContextTransitioningViewController> *toViewController = (UIViewController<SCContextTransitioningViewController> *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *containerView = [transitionContext containerView];
    [containerView addSubview:toViewController.view];
    
    NSTimeInterval transitionDuration = [self transitionDuration:transitionContext];
    
    CGRect convertedFrame = [self.contextView.superview convertRect:self.contextView.frame toView:containerView];
    CGPoint contextViewOrigin = convertedFrame.origin;
    
    self.contextView.frame = convertedFrame;
    
    [self.contextView removeFromSuperview];
    [containerView addSubview:self.contextView];
    
    CGRect contextViewToFrame = [toViewController frameForContextViewInWindowCoordinates:self.contextView];
    [self.contextView animateAlongCurveToFrame:contextViewToFrame apex:75.f duration:transitionDuration];
    
    CGRect toViewControllerFinalFrame = toViewController.view.frame;
    
    toViewController.view.alpha = 0.f;
    toViewController.view.transform = CGAffineTransformMakeScale(0.8f, 0.8f);
    toViewController.view.frame = CGRectMake(contextViewOrigin.x - CGRectGetMidX(toViewController.view.bounds),
                                             contextViewOrigin.y,
                                             CGRectGetWidth(toViewController.view.bounds), CGRectGetHeight(toViewController.view.bounds));
    
    [toViewController.view animateAlongCurveToFrame:toViewControllerFinalFrame apex:75.f duration:transitionDuration];
    
    [UIView animateWithDuration:transitionDuration
                     animations:^{
                         toViewController.view.alpha = 1.f;
                         toViewController.view.transform = CGAffineTransformIdentity;
                     } completion:^(BOOL finished) {
                         BOOL transitionWasCancelled = [transitionContext transitionWasCancelled];
                         [transitionContext completeTransition:transitionWasCancelled == NO];
                         
                         if (transitionWasCancelled == NO)
                         {
                             [toViewController insertContextViewInViewHierarchy:self.contextView];
                         }
                     }];
}

@end
