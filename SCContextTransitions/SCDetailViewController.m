//
//  SCDetailViewController.m
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import "SCDetailViewController.h"

@interface SCDetailViewController () <UIViewControllerTransitioningDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation SCDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:1.0f];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:38.f];
    self.titleLabel.text = @"Title Text";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:self.titleLabel];
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.titleLabel sizeToFit];
    self.titleLabel.frame = CGRectMake(CGRectGetMidX(self.view.bounds) - CGRectGetMidX(self.titleLabel.bounds),
                                       CGRectGetMidY(self.view.bounds) / 3.f,
                                       CGRectGetWidth(self.titleLabel.bounds), CGRectGetHeight(self.titleLabel.bounds));
}

#pragma mark - Actions

- (void)tapGestureRecognized:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - SCContextTransitioning

- (void)insertContextViewInViewHierarchy:(UIView *)contextView
{
    [self.view addSubview:contextView];
}

- (CGRect)frameForContextViewInWindowCoordinates:(UIView *)contextView
{
    CGSize contextViewSize = CGSizeMake(CGRectGetMidX(self.view.bounds), CGRectGetMidX(self.view.bounds));
    
    CGRect frameForContextView = CGRectMake(CGRectGetMidX(self.view.bounds) - (contextViewSize.width / 2.f),
                                            CGRectGetMidY(self.view.bounds) - (contextViewSize.height / 2.f),
                                            contextViewSize.width, contextViewSize.height);
    
    CGRect frameConvertedToWindow = [self.view convertRect:frameForContextView toView:nil];
    
    return frameConvertedToWindow;
}


@end
