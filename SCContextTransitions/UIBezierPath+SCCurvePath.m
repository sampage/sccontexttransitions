//
//  UIBezierPath+SCCurvePath.m
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import "UIBezierPath+SCCurvePath.h"

@implementation UIBezierPath (SCCurvePath)

+ (UIBezierPath *)curvedPathFromPoint:(CGPoint)fromPoint toPoint:(CGPoint)toPoint apex:(CGFloat)apex
{
    CGPoint controlPoint = CGPointMake((fromPoint.x + toPoint.x) / 2.f, MIN(fromPoint.y, toPoint.y) - apex);
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:fromPoint];
    [bezierPath addQuadCurveToPoint:toPoint controlPoint:controlPoint];
    
    return bezierPath;
}

@end
