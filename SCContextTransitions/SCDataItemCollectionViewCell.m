//
//  SCDataItemCollectionViewCell.m
//  SCContextTransitions
//
//  Created by sampage on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import "SCDataItemCollectionViewCell.h"

@implementation SCDataItemCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        UIView *dividingLine = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 0.5f, CGRectGetHeight(self.contentView.bounds))];
        dividingLine.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        dividingLine.backgroundColor = [UIColor darkGrayColor];
        
        [self.contentView addSubview:dividingLine];
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.contextView = nil;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.contextView.frame = [self frameForContextView];
}

- (void)setContextView:(UIView *)contextView
{
    if (_contextView.superview == self.contentView)
    {
        [_contextView removeFromSuperview];
    }
    
    _contextView = contextView;
    
    [self.contentView addSubview:_contextView];
    
    [self setNeedsLayout];
}

- (CGRect)frameForContextView
{
    CGSize contextViewSize = CGSizeMake(CGRectGetMidX(self.bounds), CGRectGetMidX(self.bounds));
    
    return CGRectIntegral(CGRectMake(CGRectGetMidX(self.contentView.bounds) - (contextViewSize.width / 2.f),
                                     CGRectGetMidY(self.contentView.bounds) - (contextViewSize.height / 2.f),
                                     contextViewSize.width, contextViewSize.height));
}

@end
