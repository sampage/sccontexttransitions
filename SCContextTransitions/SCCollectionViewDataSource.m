//
//  SCCollectionViewDataSource.m
//  SCContextTransitions
//
//  Created by sampage on 24/07/2014.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import "SCCollectionViewDataSource.h"

#import "SCDataItemCollectionViewCell.h"
#import "SCResizingCircleView.h"

NSString * kSCCollectionViewCellReuseIdentifier = @"kSCCollectionViewCellReuseIdentifier";

@interface SCCollectionViewDataSource ()

@property (nonatomic, strong) NSArray *contextViews;

@end

@implementation SCCollectionViewDataSource

- (id)init
{
    if (self = [super init])
    {
        [self generateContextViews];
    }
    return self;
}

- (void)generateContextViews
{
    NSMutableArray *mutableContextViews = [NSMutableArray array];
    
    for (int i = 0; i < 10; i++)
    {
        [mutableContextViews addObject:[[SCResizingCircleView alloc] initWithFrame:CGRectZero]];
    }
    
    self.contextViews = [mutableContextViews copy];
}

#pragma mark - Public

- (NSIndexPath *)indexPathForContextView:(UIView *)contextView
{
    NSUInteger indexOfContextView = [self.contextViews indexOfObject:contextView];
    
    return [NSIndexPath indexPathForItem:indexOfContextView inSection:0];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contextViews.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SCDataItemCollectionViewCell *collectionViewCell = (SCDataItemCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kSCCollectionViewCellReuseIdentifier forIndexPath:indexPath];
 
    collectionViewCell.contextView = self.contextViews[indexPath.item];
    
    return collectionViewCell;
}

@end
