//
//  SCResizingCircleView.m
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import "SCResizingCircleView.h"

/* See header note on deprecation */

@implementation SCResizingCircleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.contentMode = UIViewContentModeRedraw;
        self.opaque = NO;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextAddEllipseInRect(context, rect);
    CGContextSetFillColor(context, CGColorGetComponents([UIColor greenColor].CGColor));
    CGContextFillPath(context);
}

@end
