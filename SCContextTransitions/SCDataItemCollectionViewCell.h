//
//  SCDataItemCollectionViewCell.h
//  SCContextTransitions
//
//  Created by sampage on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SCDataViewGroup;
@class SCDataViewItem;

@interface SCDataItemCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIView *contextView;

- (CGRect)frameForContextView;

@end
