//
//  SCContextTransitioning.h
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SCContextTransitioningViewController <NSObject>

- (void)insertContextViewInViewHierarchy:(UIView *)contextView;
- (CGRect)frameForContextViewInWindowCoordinates:(UIView *)contextView;

@end
