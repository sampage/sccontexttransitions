//
//  SCMovesPresentTransition.h
//  SCContextTransitions
//
//  Created by Sam Page on 24/07/14.
//  Copyright (c) 2014 Subjective-C. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCContextTransitioning.h"

@interface SCMovesPresentTransition : NSObject <UIViewControllerAnimatedTransitioning, SCContextTransitioning>

@end
